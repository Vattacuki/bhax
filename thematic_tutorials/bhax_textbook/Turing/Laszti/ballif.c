#include <stdio.h>
#include <curses.h>
#include <unistd.h>

int
main ( void )
{
    WINDOW *ablak; //új ablak készítése 
    ablak = initscr ();

    int x = 0;      //x,y koordináták 
    int y = 0;

    int xnov = 1;   //x,y növelése
    int ynov = 1;

    int mx;         //oszlopok száma
    int my;         //sorok száma

    for ( ;; ) {

        getmaxyx ( ablak, my , mx ); //képernyő méretének megszerzése

        mvprintw ( y, x, "O" );     //x,y koordinátát felhasználva "O" kiiratása a képernyőre

        refresh ();                 //állandó képernyő frissítés
        usleep ( 150000 );           //milyen gyorsan haladjon a labda
	    clear();                    //törlés,hogy csak 1 "O" látszódjon       
        x = x + xnov;
        y = y + ynov;

        if ( x>=mx-1 ) { // elerte-e a jobb oldalt?
            xnov = xnov * -1;
        }
        if ( x<=0 ) { // elerte-e a bal oldalt?
            xnov = xnov * -1;
        }
        if ( y<=0 ) { // elerte-e a tetejet?
            ynov = ynov * -1;
        }
        if ( y>=my-1 ) { // elerte-e a aljat?
            ynov = ynov * -1;
        }

    }

    return 0;
}
//sudo apt-get install libncurses5-dev libncursesw5-dev kell a curses-hez
//gcc ballif.c -lncurses 
