(defun recursive (x) 
	(if ( <= x 1)
		1
		(* x ( recursive ( - x 1)))))
