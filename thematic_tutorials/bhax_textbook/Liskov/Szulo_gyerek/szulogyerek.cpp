#include <iostream>
#include <string>

using namespace std;

class Cat
	{
		public: void startMeowing()
			{

				cout << "Cat: Meow\n";

			}

};

class Kitten : public Cat
	{
		public:void tryToMeow(string miu)
			{
				cout << miu << "\n";
			}
	};

class CatAndKitten
{
	int main()
	{
		Cat* p = new Cat();
		Cat* p2 = new Kitten();

		cout << "Cat's meowing.\n";

		p->startMeowing();

		cout << "Kitten's trying to meow through mummy Cat's ref\n";

		p2->tryToMeow("I am trying but i can't.");

		delete p;
		delete p2;
	}
};
