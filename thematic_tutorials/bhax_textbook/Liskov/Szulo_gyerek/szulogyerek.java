class Cat
	{
		public void startMeowing()
			{
				System.out.println("Mummy Cat: Meow");
			}
	}

class Kitten extends Cat
	{
		public void tryToMeow(String meow)
			{
				System.out.println(meow);
			}
	}

public class CatAndKitten

{
	public static void main(String args[])
		{
			Cat p = new Cat();
			Cat p2 = new Kitten();

			System.out.println("Cat's meowing.");

			p.startMeowing();

			System.out.println("Kitten's trying to meow through mummy Cat's ref.");
			
			p2.tryToMeow("I am trying but i can't.");
		}
}
