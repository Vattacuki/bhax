// ez a T az LSP-ben
interface Madar 
{
//public:
//  void repul(){};
}

// ez a két osztály alkotja a "P programot" az LPS-ben


// itt jönnek az LSP-s S osztályok
interface RepuloMadar extends Madar 
     {
          public void repul(); 
     }

class ProgramRepul 
     {
          public static void repulj ( RepuloMadar m ) 
          {
               m.repul();

               // madar.repul(); a madár már nem tud repülni
               // s hiába lesz a leszármazott típusoknak
               // repül metódusa, azt a Madar& madar-ra úgysem lehet hívni
          }
}

interface NemRepul extends Madar
               {
                   
                    
               }

class Sas implements RepuloMadar
     {
          public void repul()
               {
                    System.out.println("Sas: Én alapból képes vagyok szárnyalni.");
               }
     }

class Pingvin implements NemRepul // ezt úgy is lehet/kell olvasni, hogy a pingvin tud repülni
     {    
            public void nemrepul()
                    {
                    System.out.println("Mégsem tudok repülni.");
                    }
     }

class LiskovraFigyel
{
     public static void main (String args[])
     {
          Program program = new Program();

          Sas sas = new Sas();
          ProgramRepul.repulj( sas );

          Pingvin pingvin = new Pingvin();
          ProgramRepul.repulj ( pingvin );

     }
}
