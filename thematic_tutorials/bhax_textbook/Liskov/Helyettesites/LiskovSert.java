// ez a T az LSP-ben
class Madar 
     {

          public void repul() 
          {
               System.out.println("Madár: Sikeresen repülök!");
          }
     }

// ez a két osztály alkotja a "P programot" az LPS-ben
class Program 
     {
          public static void fgv ( Madar madar ) 
               {
                    madar.repul();
               }    
     }

// itt jönnek az LSP-s S osztályok
class Sas extends Madar
     {
          public void repul()
               {
                    System.out.println("Sas: Én alapból képes vagyok szárnyalni.");
               }
     }

class Pingvin extends Madar // ezt úgy is lehet/kell olvasni, hogy a pingvin tud repülni
     {}

class LiskovSert 
{

     public static void main ( String args[] )
     {
          Program program = new Program();
          Madar madar = new Madar();
          program.fgv(madar);

          Sas sas = new Sas();
          program.fgv(sas);

          Pingvin pingvin = new Pingvin();
          System.out.print("Pingvin mint ");
          program.fgv(pingvin); // sérül az LSP, mert a P::fgv röptetné a Pingvint, ami ugye lehetetlen.

     }
}
