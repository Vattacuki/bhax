#ifndef LZWBINFA
#define LZWBINFA

#include <iostream>

class LZWBinFa
{
public:
	LZWBinFa();
	~LZWBinFa();


	void kiir(void);
	void kiir(std::ostream & os);
	void operator<< (char b);

	int getMelyseg();
	double getAtlag();
	double getSzoras();

	friend std::ostream & operator<< (std::ostream & os, LZWBinFa & bf);

private:
    class Csomopont
    {
    public:
    	Csomopont (char b = '/');
    	~Csomopont ();

    	Csomopont *nullasGyermek () const;
		Csomopont *egyesGyermek () const;

		void ujNullasGyermek (Csomopont * gy);
		void ujEgyesGyermek (Csomopont * gy);
		char getBetu () const;
		
	private:
		char betu;
		Csomopont *balNulla;
		Csomopont *jobbEgy;

		Csomopont (const Csomopont &);
        Csomopont & operator= (const Csomopont &); 
     };

    Csomopont *fa;
    int melyseg, atlagosszeg, atlagdb;
    double szorasosszeg;

    void kiir (Csomopont * elem, std::ostream & os);
    void szabadit (Csomopont * elem);

	LZWBinFa (const LZWBinFa &);
    LZWBinFa & operator= (const LZWBinFa &);

    Csomopont * masolo ( Csomopont * elem, Csomopont * oldtree );

protected:			// ha esetleg egyszer majd kiterjesztjük az osztályt, mert

    Csomopont gyoker;
    int maxMelyseg;
    double atlag, szoras;

    void rmelyseg (Csomopont * elem);
    void ratlag (Csomopont * elem);
    void rszoras (Csomopont * elem);
};

#endif