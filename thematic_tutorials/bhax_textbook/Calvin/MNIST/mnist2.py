"""A very simple MNIST classifier.

See extensive documentation at
http://tensorflow.org/tutorials/mnist/beginners/index.md
"""
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
#FELADAT: 
import argparse
import matplotlib as mpl
import numpy

# Import data
from tensorflow.examples.tutorials.mnist import input_data

import tensorflow as tf

import matplotlib.pyplot as plt


FLAGS = None


def readimg():
    file = tf.read_file("sajat.png")
    img = tf.image.decode_png(file, 1)
    return img


def main(_):
    model_path = "/tmp/model.ckpt"

    mnist = input_data.read_data_sets(FLAGS.data_dir, one_hot=True)

    # Create the model
    x = tf.placeholder(tf.float32, [None, 784])
    #W = tf.Variable(tf.zeros([784, 10]))
    W = tf.Variable(numpy.random.normal(0, 0.001, size=(784,10)),dtype=tf.float32)
    b = tf.Variable(tf.zeros([10]))
    y = tf.matmul(x, W) + b

    # Define loss and optimizer
    y_ = tf.placeholder(tf.float32, [None, 10])

    # The raw formulation of cross-entropy,
    #
    #   tf.reduce_mean(-tf.reduce_sum(y_ * tf.log(tf.nn.softmax(y)),
    #                                 reduction_indices=[1]))
    #
    # can be numerically unstable.
    #
    # So here we use tf.nn.softmax_cross_entropy_with_logits on the raw
    # outputs of 'y', and then average across the batch.
    cross_entropy = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(logits=y, labels=y_))
    train_step = tf.train.GradientDescentOptimizer(0.5).minimize(cross_entropy)

    # 'Saver' op to save and restore all the variables
    saver = tf.train.Saver()

    sess = tf.InteractiveSession()
    # Train
    sess.run(tf.global_variables_initializer())
    # tf.initialize_all_variables().run()
    # tf.global_variables_initializer().run
    print("-- A halozat tanitasa")
    for i in range(1000+1):
        if i in range(0,9):
            img = W[:,0]
            image = img.eval()
          #  plt.text(1, 1, "Iteracio: {}".format(i))
          #  plt.imshow(image.reshape(28, 28), cmap="hot")
          #  plt.savefig("w0.png")
          #  plt.title(i)
          #  plt.show()
    
        if i % 100 == 0 : #and i != 0:
            print(i / 10, "%")
            img = W[:,0]
            image = img.eval()
            plt.text(1, 1, "Iteracio: {}".format(i),color='white')
            plt.imshow(image.reshape(28, 28), cmap = mpl.colors.ListedColormap(['red','black','blue']))#cmap="seismic")
            plt.savefig("w0.png")
            plt.title(i)
            plt.show()

        batch_xs, batch_ys = mnist.train.next_batch(2)
        sess.run(train_step, feed_dict={x: batch_xs, y_: batch_ys})
    
    print("-"*20)

    # Test trained model
    print("-- A halozat tesztelese")
    #saver.restore(sess, model_path)
    correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
    print("-- Pontossag: ", sess.run(accuracy, feed_dict={x: mnist.test.images,
                                                          y_: mnist.test.labels}))
    print("-"*20)

    writer = tf.summary.FileWriter("/tmp/mnist_softmax_UDPROG61", sess.graph)
    img = W[:,0]
    image = img.eval()
    print("-- W_0[14,14] =", image[14*28+14])
    print("-- Egy vedesi reszfeladat az elso heten: a W_0 sulyok abrazolasa, mutatom, a tovabblepeshez csukd be az ablakat")
    #plt.imshow(image.reshape(28, 28))#, cmap=plt.cm.binary)
    plt.imshow(image.reshape(28, 28), cmap="seismic")
    plt.savefig("w0.png")
    #plt.show()
    print("----------------------------------------------------------")


    print("-- A MNIST 42. tesztkepenek felismerese, mutatom a szamot, a tovabblepeshez csukd be az ablakat")

    img = mnist.test.images[42]
    image = img
    plt.imshow(image.reshape(
        28, 28), cmap=plt.cm.binary)
    plt.savefig("4.png")
    plt.show()

    classification = sess.run(tf.argmax(y, 1), feed_dict={x: [image]})

    print("-- Ezt a halozat ennek ismeri fel: ", classification[0])
    print("----------------------------------------------------------")

    #print("-- A sajat kezi 7-esem felismerese, mutatom a szamot, a tovabblepeshez csukd be az ablakat")

    #img = mnist.test.images[41]
    #image = img
    #image = image.reshape(28 * 28)

    #plt.imshow(image.reshape(
    #    28, 28), cmap=plt.cm.binary)
    #plt.savefig("7.png")
    #plt.show()

    #classification = sess.run(tf.argmax(y, 1), feed_dict={x: [image]})

    #print("-- Ezt a halozat ennek ismeri fel: ", classification[0])
    #print("----------------------------------------------------------")

    
    print("-- A sajat kezi 4-esem felismerese, mutatom a szamot, a tovabblepeshez csukd be az ablakat")

    img = readimg()
    image = img.eval().flatten()
    for i, img in enumerate(image):
        image[i] =abs(image[i]-255)


    plt.imshow(image.reshape(28, 28), cmap=plt.cm.binary)
    plt.savefig("sajat7.png")
    plt.show()

    classification = sess.run(tf.argmax(y, 1), feed_dict={x: [image]})

    print("-- Ezt a halozat ennek ismeri fel: ", classification[0])
    print("----------------------------------------------------------")
    


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--data_dir', type=str, default='/tmp/tensorflow/mnist/input_data',
                        help='Directory for storing input data')
    FLAGS = parser.parse_args()
    tf.app.run()
