#ifndef CSOMOPONT_H
#define CSOMOPONT_H

class Csomopont {

private:
	char betu;
	Csomopont* balNulla;
	Csomopont* jobbEgy;

public:
	Csomopont(char b = '/');

	void ~Csomopont();

	Csomopont* nullasGyermek();

	Csomopont* egyesGyermek();

	void ujNullasGyermek(Csomopont* gy);

	void ujEgyesGyermek(Csomopont* gy);

	char getBetu();

private:
	Csomopont(const Csomopont& unnamed_1);
};

#endif
