#include <iostream>
#include <string>
#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>

using namespace std;
class Vizsgal {
    
private:
    string _utvonal;
    int _szamlalo = 0;
    
public:
    Vizsgal(string filePath):_utvonal(filePath) {}
    
    void bejar(boost::filesystem::path path) {
        boost::filesystem::directory_iterator it{path}, eod;
        BOOST_FOREACH(boost::filesystem::path const& p, make_pair(it, eod)) {
            if (boost::filesystem::is_regular_file(p) && boost::filesystem::extension(p.string()) == ".java") {
                cout << p << endl;
                _szamlalo++;
            }
            else if(boost::filesystem::is_directory(p)) bejar(p);
        }
    }
    
    string Utvonal() {
        return _utvonal;
    }
    
    int Mennyiseg() {
        return _szamlalo;
    }
    
};



int main(int argc, char** argv){

    Vizsgal* obj = new Vizsgal (argv[1]);
    obj->bejar(obj->Utvonal());
    cout << obj->Mennyiseg() << endl;
}

