package jdk.internal.jshell.tool.resources;

public final class version extends java.util.ListResourceBundle {
    protected final Object[][] getContents() {
        return new Object[][] {
            { "full", "11.0.4+11-post-Ubuntu-1ubuntu218.04.3" },
            { "jdk", "11.0.4" },
            { "release", "11.0.4" },
        };
    }
}
